import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		cards = new Card[52];
		
		numberOfCards = 0;
		for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cards[numberOfCards] = new Card(suit, rank);
                numberOfCards++;
            }
        }
	}
	
	public int length(){
		return numberOfCards;
	}
		
    public Card drawTopCard() {
        if (numberOfCards > 0) {
            Card topCard = cards[numberOfCards - 1];
            numberOfCards--; 
            return topCard;
        } else {
            System.out.println("Deck is empty.");
            return null;
        }
    }
	
	 public String toString() {
        String result = "";
        for (int i = 0; i < numberOfCards; i++) {
            result += cards[i].toString() + "\n";
        }
        return result;
    }
	
	public void shuffle(){
		for(int i = 0; i < this.cards.length; i++){
			int position = rng.nextInt(numberOfCards);
			Card placeHolder = this.cards[i];
			this.cards[i] = this.cards[position];
			this.cards[position] = placeHolder;
		}
	}
}